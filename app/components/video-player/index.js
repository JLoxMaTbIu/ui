export const createYoutubeVideoTemplate = (url, container) => {
  return `
    <div class="video-container">
      <iframe class="video-container__iframe" width="${container.clientWidth}" height="${container.clientHeight}" src=${url} frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      <button class="video-container__close-btn js-video-container__close-btn"></button>
    </div>
  `
}

export const initOoyalaVideo = (url, playerParam) => {
  OO.ready(() => window.pp = OO.Player.create('ooyala-video-container', url, playerParam));
}

export const createOoyalaVideoTemplate = () => {
  return `
    <div class="video-container" id="ooyala-video-container"></div>
    <button class="video-container__close-btn js-video-container__close-btn"></button>
  `;
}
export const isOoyalaVideo = (url) => url.split('').indexOf('/') === -1;

export const appendVideoPlayer = (url, container) => {
  if (isOoyalaVideo(url)) {
    const playerParam = { "skin": { "config": "//player.ooyala.com/static/v4/production/latest/skin-plugin/skin.json" } };
    const videoTemplate = createOoyalaVideoTemplate();
    container.insertAdjacentHTML('beforeend', videoTemplate);
    initOoyalaVideo(url, playerParam);
  } else {
    const videoTemplate = createYoutubeVideoTemplate(url, container);
    container.insertAdjacentHTML('beforeend', videoTemplate);
  }
}

export const closeVideoPlayer = () => {
  const videoPlayer = document.querySelector('.video-container');
  const closeVideoButton = document.querySelector('.js-video-container__close-btn')
  closeVideoButton.remove();
  videoPlayer.remove();
}

const videoContainerSelector = document.querySelector('.main-slider');

videoContainerSelector.addEventListener('click', function (e) {
  const target = e.target;
  if (target.classList.contains('js-video-button')) {
    const url = target.getAttribute('data-video-source-url');
    videoPlayer.appendVideoPlayer(url, this);
  }
  target.classList.contains('js-video-container__close-btn') && videoPlayer.closeVideoPlayer();
});