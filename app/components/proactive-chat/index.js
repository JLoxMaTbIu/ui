/**
 * @namespace ProactiveChat
 * @description This module is responsible for ProactiveChat logic
 */

/** @global ProActive chat section */
var $proactiveChat = $('.pro-active-chat');

/** @global node which close ProActive chat */
var $closeChatBtn = $('.js-close-chat');

/** @global node which opens ProActive chat */
var $openChatBtn = $('.js-toggle-chat');

/**
 * @handler
 * @description open ProActive chat component
 */
$openChatBtn.on('click', function () {
  $(this).parent().addClass('is-opened');
  $(this).hide();
});

/**
 * @handler
 * @description close ProActive chat component
 */
$closeChatBtn.on('click', function () {
  var $parentContainer = $(this).parent().parent().parent();
  $parentContainer.removeClass('is-opened');
  $parentContainer.find('.js-toggle-chat').show();
});
