import * as Tingle from 'tingle.js';

const newsLetterModalContent = document.querySelector('.popup').outerHTML;
const subscribeBtn = document.querySelector('.js-subscribe-btn');

export const modal = new Tingle.modal({
  cssClass: ['popup-wrapper']
});

modal.setContent(newsLetterModalContent);

const closeModalBtn = document.querySelector('.popup-wrapper .js-close-popup');

closeModalBtn.addEventListener('click', () => {
  modal.close();
});