const socialNetworksObject = {
  "facebook": "https://www.facebook.com/sharer/sharer.php?u=",
  "twitter": "https://twitter.com/share?url=",
  "linkedin": "https://www.linkedin.com/shareArticle?mini=true&url=",
  "gplus": "https://plus.google.com/share?url=",
  "weibo": "http://service.weibo.com/share/share.php?url=",
  "we-chat": "weixin://dl/posts?url="
}

const printWebPage = () => window.print && window.print();
const setMailHref = ($mailNode) => {
  const mailSubject = encodeURIComponent($mailNode.attr('data-additional-text') || document.title);
  const mailBody = encodeURIComponent($mailNode.attr('data-sharetext') || siteUrl);
  $mailNode.attr(`href`, `mailto:?subject=${mailSubject}&body=${mailBody}`);
}

const siteUrl = window.location.href === "http://localhost:3000/" ? encodeURIComponent(`https://www.schneider-electric.co.uk/en/all-products/`) : window.location.href;
const $printButton = $('.social-share__list-link[data-type="print"]');
const $mailToLink = $('.social-share__list-link[data-type="mail"]')

const sendToSocialNetwork = (network) => {
  const $networkNode = $(`.social-share__list-link[data-type="${network}"]`);
  const sharetext = encodeURIComponent($networkNode.attr('data-sharetext')) || ``;
  const additionalText = encodeURIComponent($networkNode.attr('data-additional-text')) || ``;
  const title = encodeURIComponent($networkNode.attr('data-title')) || ``;
  const targetUrl = `${socialNetworksObject[network]}${siteUrl}&text=${sharetext}&title=${title}`;
  $networkNode.attr('href', targetUrl);
}

Object.keys(socialNetworksObject).map(el => sendToSocialNetwork(el));

$printButton.on('click', printWebPage);
setMailHref($mailToLink);