import seAppCommonFunctions from './seAppCommonFunctions.js';
const getElementBottomPosition = el => $(el).position().top + $(el).offset().top + $(el).outerHeight(true);
const getElementTopPosition = el => $(el).offset().top;
const stickyItemsSelectorsArray = ['.support-bar', '.pro-active-chat', '.socials-share'];
const $stickyItems = $(stickyItemsSelectorsArray.join(','));
const $mainContentSelector = $('.se-page-content');
const mainContentTopOffset = getElementTopPosition($mainContentSelector);
const mainContentBottomOffset =  $mainContentSelector.offset().top + $mainContentSelector.outerHeight(true);

const computeElementsFullOffset = (nodes) => {
  return nodes.reduce((acc,el) => {
    acc.push({
        selector: $(el),
        bottomPosition: getElementBottomPosition(el)
      }
    );
    return acc;
  }, []);
}

const defineElementsStopScrollingPosition = (nodes, stopScrollingPosition) => {
  nodes.forEach(el => {
    el.bottomPosition >= stopScrollingPosition ? $(el.selector).removeClass('is-sticked').addClass('is-sticked-to-footer') :
    $(el.selector).removeClass('is-sticked-to-footer').addClass('is-sticked')
  });
}

console.log($('.se-page-content').position().top);
console.log($('.se-page-content').offset().top);
console.log($('.se-page-content').outerHeight(true));

function stickElementsWhenScrolling() {
  let currentScrollPosition = $(this).scrollTop();
  let currentElementsOffsetObject = computeElementsFullOffset(stickyItemsSelectorsArray);
  if (currentScrollPosition > mainContentTopOffset) {
    defineElementsStopScrollingPosition(currentElementsOffsetObject, mainContentBottomOffset);
  } else {
    $stickyItems.removeClass('is-sticked').removeClass('is-sticked-to-footer');
  }
}
// if (currentScrollPosition > mainContentBottomOffset - 40) {
//   $stickyItems.removeClass('is-sticked').addClass('is-sticked-to-footer')
// } else {
//   $stickyItems.removeClass('is-sticked-to-footer').addClass('is-sticked')
// }
$(window).on('scroll', stickElementsWhenScrolling);

// function onElementHeightChange(elm, callback){
//     var lastHeight = elm.clientHeight, newHeight;
//     (function run(){
//         newHeight = elm.clientHeight;
//         if( lastHeight != newHeight )
//             callback();
//         lastHeight = newHeight;

//         if( elm.onElementHeightChangeTimer )
//             clearTimeout(elm.onElementHeightChangeTimer);

//         elm.onElementHeightChangeTimer = setTimeout(run, 200);
//     })();
// }


// onElementHeightChange(document.body, function(){
//     alert('Body height changed');
// });