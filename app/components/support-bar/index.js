const $supportBarToggler = $('.js-toggle-support');

$supportBarToggler.on('click', function() {
  $(this).closest('.support-bar').toggleClass('is-expanded');
})