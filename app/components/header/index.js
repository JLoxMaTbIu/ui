export const Header = (() => {
  $(window).on('scroll', checkHeaderState);

  $(window).on('resize', checkHeaderState);

  function checkHeaderState() {
    const offset = $('.smartbanner').outerHeight();
    const headerHeight = $('.js-header').outerHeight();

    if ($(window).scrollTop() >= offset || 0) {
      $('.js-header-wrapper').height(headerHeight);
      $('.js-header').addClass('is-fixed');
    } else {
      $('.js-header').removeClass('is-fixed');
      $('.js-header-wrapper').css('height', '');
    }
  }
})();
