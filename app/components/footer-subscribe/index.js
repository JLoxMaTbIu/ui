import Choices from 'choices.js';

const footerSubscribeDropdown = document.querySelector('.footer-subscribe-dropdown');
const footerDropdownConfig = {
  silent: false,
  choices: [],
  placeholder: true,
  placeholderValue: 'I am a...',
  searchEnabled: false,
  itemSelectText: '',
  shouldSort: false,
  shouldSortItems: false,
  classNames: {
    containerOuter: 'common-custom-select',
    containerInner: 'common-custom-select__inner',
    input: 'common-custom-select__input',
    inputCloned: 'common-custom-select__input--cloned',
    list: 'common-custom-select__list',
    listItems: 'common-custom-select__list--multiple',
    listSingle: 'common-custom-select__list--single',
    listDropdown: 'common-custom-select__list--dropdown',
    item: 'common-custom-select__item',
    itemSelectable: 'common-custom-select__item--selectable',
    itemDisabled: 'common-custom-select__item--disabled',
    itemChoice: 'common-custom-select__item--choice',
    placeholder: 'common-custom-select__placeholder',
    group: 'common-custom-select__group',
    groupHeading: 'common-custom-select__heading',
    button: 'common-custom-select__button',
    activeState: 'is-active',
    focusState: 'is-focused',
    openState: 'is-open',
    disabledState: 'is-disabled',
    highlightedState: 'is-highlighted',
    hiddenState: 'is-hidden',
    flippedState: 'is-flipped',
    loadingState: 'is-loading',
    noResults: 'has-no-results',
    nofooterChoices: 'has-no-common-custom-select'
  }
}

export const footerSubscribe = new Choices(footerSubscribeDropdown, footerDropdownConfig);