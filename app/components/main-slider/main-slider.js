import Swiper from 'swiper';

const sliderConfig = {
  speed: 300,
  longSwiper: true,
  navigation: {
    nextEl: '.main-slider .swiper-button-next',
    prevEl: '.main-slider .swiper-button-prev'
  },
  pagination: {
    el: '.main-slider .swiper-pagination',
    type: 'bullets',
    clickable: true
  }
};

const mainSlider = new Swiper('.main-slider .swiper-container', sliderConfig);