export const CountrySelectCountries = (() => {
  $('.js-country-dd-info').on('click', '.js-choose-country', function () {
    const countrySelect = $('.js-country-dd');

    $(this).toggleClass('is-active');

    if (!$(this).hasClass('is-active')) {
      closeCountryListSectionSelection(countrySelect);
    } else {
      openCountryListSectionSelection(countrySelect);
    }
  });

  $('.js-country-dd-list-section').on('click', '.js-close-country-list', function () {
    const countrySelect = $('.js-country-dd');

    closeCountryListSectionSelection(countrySelect);
  });

  /** Opens countrySelect country list section
   * @function
   * @name openCountryListSectionSelection */
  function openCountryListSectionSelection(countrySelect) {


    const countrySelectListSection = countrySelect.find('.js-country-dd-list-section');

    if (countrySelect.hasClass('is-opened') && !countrySelectListSection.hasClass('is-opened')) {

      disableScrolling();

      countrySelectListSection.addClass('is-opened');
      var offsetValue = $('.js-metabar').outerHeight() + $('.js-header-content').outerHeight();
      if ($('.smartbanner')) {
        offsetValue += $('.smartbanner').outerHeight();
      }

      // position countrySelectListSection
      countrySelectListSection.css({'top': offsetValue});

      // show countrySelectListSection with animation
      countrySelectListSection.slideDown(300);
    }
  }

  /** Closes countrySelect country list section
   * @function
   * @name closeCountryListSectionSelection */
  function closeCountryListSectionSelection(countrySelect) {
    const countrySelectListSection = countrySelect.find('.js-country-dd-list-section');

    countrySelect.find('.js-choose-country').removeClass('is-active');

    if (countrySelectListSection.hasClass('is-opened')) {
      countrySelectListSection.removeClass('is-opened');
    }

    enableScrolling();

    // removes styles of countrySelectRegions and countrySelectList
    // after countrySelectListSection hides with animation
    countrySelectListSection.slideUp(300, function () {
      // countrySelectRegions.removeAttr('style');
      countrySelectListSection.removeAttr('style');
    });
  }

  // Disables page scrolling
  function enableScrolling() {
    $('html').css({'overflow': ''});
    $('body').css({'overflow-y': 'auto'});
  }

  // Disables page scrolling
  function disableScrolling() {
    $('html').css({'overflow': 'hidden'});
    $('body').css({'overflow-y': 'hidden'});
  }
})();