import countrySelectCountries from './country-select-countries';

export const CountrySelect = (() => {
  // Listener for metabar toggler
  $('.js-metabar-item').on('click', '.js-open-country-select', function () {
    const countrySelect = $('.js-country-dd');

    changeCountrySelectState(countrySelect);
  });

  // Listener for country select close button
  $('.js-country-dd-info').on('click', '.js-close-country-dd', function () {
    const countrySelect = $('.js-country-dd');

    changeCountrySelectState(countrySelect);
  });

  // Changes country select state
  function changeCountrySelectState(countrySelect) {
    const trigger = $('.js-open-country-select').closest('.js-metabar-item');

    toggleCountrySelectTrigger(trigger);

    if (countrySelect.hasClass('is-opened')) {
      closeCountrySelect(countrySelect);
    } else {
      openCountrySelect(countrySelect);
    }
  }

  // Toggles triggers class
  function toggleCountrySelectTrigger(trigger) {
    trigger.toggleClass('is-active');
  }

  // Opens country select
  function openCountrySelect(countrySelect) {
    countrySelect.addClass('is-opened');
    countrySelect.slideDown(300);
  }

  // Closes country select
  function closeCountrySelect(countrySelect) {
    countrySelect.removeClass('is-opened');
    countrySelect.slideUp(300);
  }


  /** Opens countrySelect country list section
   * @function
   * @name openCountryListSectionSelection */
  function openCountryListSectionSelection(countrySelect) {
    const listSection = countrySelect.find('.js-country-dd-list-section');
    if (countrySelect.hasClass('is-opened') && !listSection.hasClass('is-opened')) {
      // disable page scrolling
      disableScrolling();

      listSection.addClass('is-opened');

      // show listSection with animation
      listSection.slideDown(300);
    }
  }

  // Disables page scrolling
  function disableScrolling() {
    $('html').css({'overflow': 'hidden'});
    $('body').css({'overflow-y': 'hidden'});
  }
})();

