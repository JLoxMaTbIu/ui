/**
 * @namespace SVGtoLocalStorage
 * @description
 * On the very first load of the website this code reads the contents of a SVG file and inserts the data into the document,
 * puts the data into localStorage, on every following load it reads the data from localStorage and inserts the SVG data in the document.
 *
 * If localStorage is not supported, is disabled or overfilled, the script still reads the contents of a SVG file and inserts the data in the document.
 *
 * Source URL: https://osvaldas.info/caching-svg-sprite-in-localstorage
 */

/**
 * @memberOf SVGtoLocalStorage
 * @function
 * @name Immediately Invoked Function Expressions
 * @param {Object} window object represents the browser's window
 * @param {Object} document object represents loaded web page in the browser
 */

( function (window, document) {
  'use strict';

  /** Creates path to the SVG symbol spite depends on environment(INT,SQE,PRD or php prototype) */
  var fileURL = window.location.origin !== 'http://localhost:3000' ? `${window.location.origin}/svg-sprite/svg-symbols/svg-symbols.svg` : './../svg-sprite/svg-symbols/svg-symbols.svg';
console.log(fileURL);
  /** Format date, dd mm yy without spaces, of last update for svg sprite symbol */
  var revision = '03042018';
  var isLocalStorage = 'localStorage' in window && window['localStorage'] !== null;
  var request;
  var data;
  var insertIT = function () {
    document.body.insertAdjacentHTML('afterbegin', data);
  };
  var insert = function () {
    if (document.body) {
      insertIT();
    } else {
      document.addEventListener('DOMContentLoaded', insertIT);
    }
  };

  if (!document.createElementNS || !document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect) {
    return true;
  }

  /** Check if SVG sprite symbol in localStorage if yes insert it to the psge  */
  if (isLocalStorage && localStorage.getItem('inlineSVGRevision') === revision) {
    data = localStorage.getItem('inlineSVGSpriteData');
    if (data) {
      insert();
      return true;
    }
  }

  try {
    /**
     * A XHR request to retrieve file and put it into localStorage
     * @memberOf SVGtoLocalStorage
     * @function
     * @name XMLHttpRequest
     * @param {string} method 'GET'
     * @param {string} path to the SVG file
     * @param {boolean} async An optional boolean parameter, defaulting to true, indicating whether or not to perform the operation asynchronously.
     * If this value is false, the send()method does not return until the response is received.
     */

    request = new XMLHttpRequest();
    request.open('GET', fileURL, true);
    request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        data = request.responseText;
        insert();
        if (isLocalStorage) {
          localStorage.setItem('inlineSVGSpriteData', data);
          localStorage.setItem('inlineSVGRevision', revision);
        }
      }
    };
    request.send();
  } catch (e) {
    console.error('ERROR in svg-localstorage.js : ' + e);
  }
}(window, document) );
