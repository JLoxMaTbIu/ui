class seAppCommonFunctions {
  static showCookieMessage() {
    const notificationBlock = document.querySelector('.js-cookie-notification');

    if (document.cookie.indexOf('display_cookie') >= 0) {
      notificationBlock.remove();
    } else {
      notificationBlock.classList.add('is-cookie-opened');
    }

    notificationBlock.addEventListener('click', function confirmationLink(e) {
      let eTarget = e.target;

      if (eTarget && eTarget.matches('.js-close-notification')) {
        event.preventDefault();
        document.cookie = 'display_cookie=1';
        notificationBlock.removeEventListener('click', confirmationLink);
        notificationBlock.remove();
      }
    }, false);
  }

  static throttle(fn, wait) {
    var time = Date.now();
    return function() {
      if ((time + wait - Date.now()) < 0) {
        fn();
        time = Date.now();
      }
    }
  }
}

export default seAppCommonFunctions;
