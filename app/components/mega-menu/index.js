$(function () {
  var $submenuToggler = $('.js-mm-item');
  var $categoryToggler = $('.js-mm-cat');
  var $metabar = $('.metabar');

  $submenuToggler.on('click', function (e) {
    e.preventDefault();
    var target = e.target;
    if (target.classList.contains('mm-item__title')) {
      $(this).toggleClass('mm-item--expanded');
      if ($submenuToggler.hasClass('mm-item--expanded')) {
        $submenuToggler.not('.mm-item--expanded').hide()
        $metabar.hide();
      } else {
        $submenuToggler.show();
        $metabar.show();
      }
    }
  });

  $categoryToggler.on('click', function (e) {
    e.preventDefault();
    var target = e.target;
    let svgIconId = $(this).find('.mm-cat__title svg use');
    if (target.classList.contains('mm-cat__title')) {
      $(this).toggleClass('mm-cat--expanded');
      $(this).hasClass('mm-cat--expanded') ? svgIconId.attr('xlink:href', '#minus') : svgIconId.attr('xlink:href', '#plus');
    }
  });
});