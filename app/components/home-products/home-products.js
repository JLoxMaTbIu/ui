import Swiper from 'swiper';

const homeProductsSliderConfig = {
  speed: 300,
  longSwiper: true,
  slidesPerView: 5,
  spaceBetween: 0,
  navigation: {
    nextEl: '.home-products .swiper-button-next',
    prevEl: '.home-products .swiper-button-prev',
  },
  breakpoints: {
    980: {
      slidesPerView: 1,
      spaceBetween: 10
    }
  }
}

const homeProductsSlider =  new Swiper('.home-products .swiper-container', homeProductsSliderConfig);