import Swiper from 'swiper';

const simpleSliderConfig = {
  speed: 400,
  longSwiper: true,
  autoHeight: true,
  pagination: {
    el: '.simple-slider .swiper-pagination',
    type: 'bullets',
    clickable: true
  },
  spaceBetween: 20
}

const simpleSlider =  new Swiper('.simple-slider .swiper-container', simpleSliderConfig);