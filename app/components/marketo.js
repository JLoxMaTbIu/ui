import { modal } from './newsletter-modal';

const grabMarketoRequestParams = (form) =>
  [...form.find('input[type="hidden"]')].reduce((acc, el) => {
    acc[$(el).attr('name')] = $(el).attr('value');
    return acc;
  }, {});

const grabFooterFormParams = (form) => {
  const formId = form.find('input[name="pageUri"]').val();
  const responsysTcmId = form.find('input[name="responsys-settings"]').val();
  const eMail = form.find('.footer-subscribe-field').val();
  const iAmDropdwon = form.find('.footer-subscribe-dropdown').val();

  return {
    "pageUri": formId,
    "responsys-settings": responsysTcmId,
    "email": eMail,
    "MW_CUSTOMER_TYPE": iAmDropdwon
  };
}

$('.js-subscribe-btn').on('click', function (e) {
  e.preventDefault();
  const $form = $('.footer-subscribe-form');
  const responsysParams = grabFooterFormParams($form);
  const marketoParams = grabMarketoRequestParams($form);
  const formFullData = Object.assign(marketoParams, responsysParams);
  const websiteUrl = `http://www-int1.staging.schneider-electric.com/us/en/`;
  const trimmedUrl = websiteUrl.replace(/\/+$/, "");
  const responsysPostUrl = `${trimmedUrl}/sdltfosvc/responsys/submit.request`;
  const marketoPostUrl = `${trimmedUrl}/sdltfosvc/marketo/submit.request`;

  $.post(responsysPostUrl, responsysParams).done((result) => {
    console.log("responsys request post success");

    $.post(marketoPostUrl, formFullData, null).done(function (result1) {
      console.log("marketo request post success");
      modal.open();
    })
    .fail((result1) => {
      console.log("marketo request post failed : ");
      console.log(result1);
    });
  })
  .fail((result) => {
    console.log("responsys request post failed : ");
    console.log(result);
  });
})