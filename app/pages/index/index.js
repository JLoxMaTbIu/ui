import './index.less';
import './../../components/marketo';
import './../../components/socials-share/socials-share.js';
import './../../components/svg-localstorage';
import './../../components/mega-menu';
import './../../components/newsletter-modal'
import './../../components/proactive-chat'
import './../../components/main-slider/main-slider.js';
import './../../components/support-bar';
import './../../components/simple-slider/simple-slider.js';
import './../../components/home-products/home-products.js';
import './../../components/sticky-items.js';
import seAppCommonFunctions from './../../components/seAppCommonFunctions.js';
import SmartBanner from './../../../node_modules/smartbanner.js/dist/smartbanner.js';

import {
  header,
  footerSubscribe,
  videoPlayer
} from 'Components';

seAppCommonFunctions.showCookieMessage();
