import './typography.less';

import Swiper from 'swiper';
import seAppCommonFunctions from './../../components/seAppCommonFunctions.js';
//import { createHTML } from './../../components/common.js';
import { sliderConfig } from './../../components/main-slider/main-slider.js';
import { simpleSliderConfig } from './../../components/simple-slider/simple-slider.js';
import { homeProductsSliderConfig } from './../../components/home-products/home-products.js';
import SmartBanner from './../../../node_modules/smartbanner.js/dist/smartbanner.js';

import {
  header,
  footerSubscribe,
  modal,
  videoPlayer
} from 'Components';


const mainSlider = new Swiper('.main-slider .swiper-container', sliderConfig);
const simpleSlider =  new Swiper('.simple-slider .swiper-container', simpleSliderConfig);
const homeProductsSlider =  new Swiper('.home-products .swiper-container', homeProductsSliderConfig);

const videoContainerSelector = document.querySelector('.main-slider');
const subscribeBtn = document.querySelector('.js-subscribe-btn');
const closeModalBtn = document.querySelector('.js-close-popup');

subscribeBtn.addEventListener('click', (e) => {
  e.preventDefault();
  modal.open();
});
closeModalBtn.addEventListener('click', () => {
  modal.close();
});

videoContainerSelector.addEventListener('click', function (e) {
  const target = e.target;
  if (target.classList.contains('video-button')) {
    const url = target.getAttribute('data-video-source-url');
    videoPlayer.appendVideoPlayer(url, this);
  }
  target.classList.contains('js-video-container__close-btn') && videoPlayer.closeVideoPlayer();
});

seAppCommonFunctions.showCookieMessage();

