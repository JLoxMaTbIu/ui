# README #

Frontend Prototype Refactoring (SDL 8.5)

### What is this repository for? ###

This repo will contain new frontend approach, which will be used after migrating SDL 2013 version to newest SDL 8.5. After migrating all existing HTML components, CSS styles, Typography, and JS files will be refactored.

### How do I get set up? ###

```
git clone https://<your_email_here>/schneideripo/se-sdl-ui.git

cd se-sdl-ui

npm install
```

### Create blocks

```
npm run make-block [block-name] [block-name]
```

### Contribution guidelines ###

We are using git flow branching strategy. So, every new feauture should developed in new branch. After implementing feature you should create pull request in main branch(develop) and set at least 2 reviewers.

### Who do I talk to? ###

* Yahor Simak - [Yahor_Simak@epam.com](mailto:Yahor_Simak@epam.com)