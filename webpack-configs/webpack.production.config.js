import webpack from 'webpack';
import Config from 'webpack-config';
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin")

const pathsToClean = [
  './../dist/*.*',
  './../dist/**/*.*'
];

const cleanOptions = {
  verbose: true,
  allowExternal: true
};

export default new Config().extend('webpack-configs/webpack.base.config.js').merge({
  devtool: 'source-map',
  module: {
    rules: [
      /* js */
      {
        test: /\.js$/,
        exclude: /node_modules\/(?!(dom7|swiper)\/).*/,
        use: [
          {
            loader: 'babel-loader'
          }
          // { loader: 'eslint-loader' }
        ]
      },
      /* less */
      {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                // minimize: true,
                importLoaders: 1
              }
            },
            {
              loader: 'postcss-loader',
            },
            {
              loader: 'less-loader',
            }
          ]
        })
      },
      /* css */
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                // minimize: true,
                importLoaders: 1
              }
            },
            {
              loader: 'postcss-loader',
            }
          ]
        })
      },
      /* images */
      {
        test: /\.(png|jpg|svg|webp)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: '/images/',
              publicPath: '../',
              useRelativePath: true,

            }
          },
          //TODO: choose best configuration, not working now
          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true,
                quality: 65
              },
              optipng: {
                enabled: false,
              },
              pngquant: {
                quality: '65-90',
                speed: 4
              },
              gifsicle: {
                interlaced: false,
              },
              webp: {
                quality: 75
              }
            }
          }
        ]
      },
      /* fonts */
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
              publicPath: '../'
            }
          }
        ]
      },
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      cache: true,
      compress: {
        warnings: true
      }
    }),
    new CopyWebpackPlugin([{
      from: 'app/images/',
      to: '/images/'
    }]),
    new CleanWebpackPlugin(pathsToClean, cleanOptions),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      filename: 'js/common.js',
      minChunks: 2
    }),
    new ExtractTextPlugin({ filename: 'css/[name].css' })
  ]
});
