import webpack from 'webpack';
const path = require('path');
const fs = require('fs');
let HtmlWebpackPlugin = require('html-webpack-plugin');

/**
 * Generate entry points object. We construct this object according to files in pages/ directory
 * @param entryPointsDir
 * @returns {Object}
 */
const generateEntryPoints = (entryPointsDir) => {
  const templates = fs.readdirSync(path.resolve(__dirname, '../', entryPointsDir));
  return templates.reduce( (acc, item) => {
    return {
      ...acc,
      [item]: `./app/pages/${item}/${item}.js`
    };
  }, {} );
};

/**
 * Generate array of HtmlWebpackPlugin objects to have multiple html-pages after assembly
 * @param templateDir
 * @returns {Object}
 */
const generateHtmlPlugins = (templateDir) => {
  const templates = fs.readdirSync(path.resolve(__dirname, '../', templateDir));
  return templates.map(item => {
    return new HtmlWebpackPlugin({
      page: require(path.resolve(__dirname, '../', `app/pages/${item}/data.json`)),
      filename: `${item}.html`,
      chunks: [ 'common', `${item}` ],
      template: path.resolve(__dirname, '../', `app/pages/${item}/${item}.hbs`)
    });
  });
};

/* ------------------------------------------------------------------------------ */

module.exports = {
  entry: generateEntryPoints('./app/pages'),
  // common: './components/common.js',
  output: {
    filename: 'js/[name].js',
    path: path.join(__dirname, './../dist'),
    publicPath: './'
  },
  module: {
    rules: [
      /* handlebars */
      {
        test: /\.hbs$/,
        use: [
          {
            loader: 'handlebars-loader'
          }
        ]
      },
      /* svg sprites */
      {
        test: /\.svg$/,
        include: [
          path.resolve(__dirname, './../app/svg-sprite')
        ],
        loader: 'svg-sprite-loader'
      }
    ]
  },
  plugins: [
    function () {
      this.plugin('watch-run', function (watching, callback) {
        console.log('* Begin compile at ' + new Date());
        callback();
      });
    },
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      filename: 'js/common.js',
      minChunks: 2
    }),
    new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV) }),
    ...generateHtmlPlugins('./app/pages')
  ],
  resolve: {
    alias: {
      Utils: path.resolve(__dirname, './../app/components/_utils/'),
      Base: path.resolve(__dirname, './../app/components/_base/'),
      Components: path.resolve(__dirname, './../app/components/')
    }
  }
};
