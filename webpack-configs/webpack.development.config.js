import Config from 'webpack-config';
const path = require('path');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlReloadPlugin = require('reload-html-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');

const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 8080;
const PROXY = `http://${HOST}:${PORT}`;

export default new Config().extend('webpack-configs/webpack.base.config.js').merge({
  devtool: 'inline-source-map',
  output: {
    publicPath: '/'
  },
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules\/(?!(dom7|swiper)\/).*/,
        use: [
          { loader: 'babel-loader' }
        ]
      },
      {
        test: /\.less$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader',
          },
          {
            loader: 'less-loader',
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'postcss-loader',
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          }
        ]
      },
      /* images */
      {
        test: /\.(png|jpg|svg)$/,
        include: [
          path.resolve(__dirname, './../app/images')
        ],
        exclude: [
          path.resolve(__dirname, './../app/svg-sprite')
        ],
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: './../app/images/',
            }
          }
        ]
      },
      /* fonts */
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      },
    ]
  },
  plugins: [
    new HtmlReloadPlugin(),
    new WebpackNotifierPlugin(),
    new BrowserSyncPlugin(
      {
        host: '10.6.206.59',
        port: 3000,
        proxy: PROXY,
        notify: false
      },
      { reload: false }
    )
  ]
});
