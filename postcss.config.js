module.exports = ({ file, options, env }) => ({
    parser: file.extname === '.sss' ? 'sugarss' : false,
    plugins: {
        'autoprefixer': {
            browsers: ['last 2 versions', '> 5%']
        }
    }
});
