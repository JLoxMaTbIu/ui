const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const rtlcss = require('gulp-rtlcss');
const rename = require('gulp-rename');
const replace = require('gulp-replace');
const runSequence = require('run-sequence');
const htmlbeautify = require('gulp-html-beautify');
const intercept = require('gulp-intercept');
const svgSymbols = require('gulp-svg-symbols');
const svgmin = require('gulp-svgmin');

const styleSheetRegExp = /href=("|').*?([\w.]+\.css)\1/gi;
const ltrDocType = '<html lang="en" dir="ltr">';
const rtlDocType = '<html lang="en" dir="rtl">';

const handleFile = require('./app/gulp-jsp-compiler/transformMarkup');

var config = {
    svgIconsSrc: './app/svg-sprite/sprite/*.svg',
    svgIconsMinDest: './app/svg-sprite/sprite/minified/',
    svgFlagsSrc: './app/svg-sprite/flags/*.svg',
    svgFlagsMinDest: './app/svg-sprite/flags/minified/',
    svgSymbolSpriteSrc: './app/svg-sprite/sprite/minified/*.svg',
    svgSymbolSpriteDest: './app/svg-sprite/svg-symbols/',
    svgSymbols: {
        svgAttrs: {
            class: 'svg-sprite',
            style: 'display: none;'
        }
    }
  };

gulp.task('svg:spriteSymbol', function () {
    gulp.src(config.svgSymbolSpriteSrc)
      .pipe(svgSymbols(config.svgSymbols))
      .pipe(gulp.dest(config.svgSymbolSpriteDest));
  });

  gulp.task('svg:flagsMinify', function () {
    gulp.src(config.svgFlagsSrc)
      .pipe(svgmin())
      .pipe(gulp.dest(config.svgFlagsMinDest));
  });

  // Start of development tasks
  gulp.task('svg:iconsMinify', function () {
    return gulp.src(config.svgIconsSrc)
      .pipe(svgmin())
      .pipe(gulp.dest(config.svgIconsMinDest));
  });

gulp.task('create-rtl-styles', () => {
    gulp.src(['dist/css/*.css'])
        .pipe(rtlcss())
        .pipe(rename({ suffix: '-rtl' }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('create-rtl-html', () => {
    gulp.src(['dist/*.html'])
        .pipe(rename({ suffix: '-rtl' }))
        .pipe(replace(ltrDocType, rtlDocType))
        .pipe(replace(styleSheetRegExp, (match) => `${match.split('.css')[0]}-rtl.css"`))
        .pipe(htmlbeautify({ indentSize: 2 }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('jsp-compile', () => {
    gulp.src('./components/**/*.hbs')
        .pipe(intercept(handleFile))
        .pipe(rename({extname: '.jsp'}))
        .pipe(gulp.dest('./dist/jsp-components/'));
  });

gulp.task('create-rtl', function () {
    runSequence('create-rtl-html', 'create-rtl-styles', 'jsp-compile');
});

gulp.task('svg-sprite', function () {
    runSequence('svg:flagsMinify', 'svg:iconsMinify', 'svg:spriteSymbol');
});
